package pl.edu.agh.sr.blackjack.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import pl.edu.agh.sr.blackjack.api.BlackJack;

public class Client {

	private static final String HELP = "Options: \n[0] Start game.\n[1] "
			+ "Hit.\n[2] Stand.\n[3] Double down.\n[4] "
			+ "Split.\n[5] Insurance.\n[6] Exit.\n[7] This message.\n";

	public static void main(String[] args) {

		if (args.length != 4) {
			System.out.println("Usage: java Client host port nick money");
			System.exit(0);
		}

		StringBuilder buffer = new StringBuilder("rmi://");
		buffer.append(args[0]);
		buffer.append(":");
		buffer.append(args[1]);
		buffer.append("/");

		String nick = args[2];
		int money = Integer.parseInt(args[3]);

		try {
			BlackJack blackjack = (BlackJack) Naming.lookup(buffer.toString()
					+ "blackjack");

			ListenerImpl listener = new ListenerImpl(money);

			UnicastRemoteObject.exportObject(listener, 0);

			blackjack.reigster(nick, listener);

			System.out.println("Hi! " + nick + "\nLet's play a game!\n");
			Client.help();

			BufferedReader input = new BufferedReader(new InputStreamReader(
					System.in));

			String line;

			while ((line = input.readLine()) != null) {
				if (!blackjack.isStand(nick)) {
					try {
						int option = 7;
						if (line.length() >= 1)
							option = Integer.parseInt(line.substring(0, 1));
						switch (option) {
						case 0:
							System.out.println("Enter a bid: ");
							int bid = Integer.parseInt(input.readLine());
							if (bid <= money) {
								blackjack.startGame(nick, bid);
							} else
								System.out.println("You dont have enough cash");
							break;
						case 1:
							blackjack.hit(nick);
							break;
						case 2:
							blackjack.stand(nick);
							break;
						case 3:
							blackjack.doubleDown(nick);
							break;
						case 4:
							blackjack.split(nick);
							break;
						case 5:
							blackjack.insurence(nick);
							break;
						case 6:
							blackjack.unregister(nick);
							System.exit(0);
						case 7:
							Client.help();
							break;
						default:
							Client.help();
						}
					} catch (NumberFormatException e) {
						System.err.print("Bad argument.\n");
						help();
					} catch (UnsupportedOperationException e) {
						System.err.println("Not implemented yet.");
					}
				}
				if (blackjack.isGameEnd() || listener.isGameEnd()) {
					money = listener.getMoney();
					listener.setGameEnd(false);
					if (money == 0)
						System.out.println("GAME OVER!");
					System.out.println("\nYou have " + money
							+ "$\nDo you want play another game? y/n");
					line = input.readLine();
					if (line.startsWith("y"))
						continue;
					else {
						blackjack.unregister(nick);
						System.exit(0);
					}
				}
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void help() {
		System.out.print(HELP);
	}

}
