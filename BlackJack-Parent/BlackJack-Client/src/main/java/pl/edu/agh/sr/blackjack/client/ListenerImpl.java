package pl.edu.agh.sr.blackjack.client;

import java.rmi.RemoteException;

import pl.edu.agh.sr.blackjack.api.Listener;

public class ListenerImpl implements Listener {

	private int money;
	private boolean gameEnd;

	public ListenerImpl(int money) {
		this.money = money;
	}

	public void onNewAction(String msg) throws RemoteException {
		System.out.print(msg);
	}

	public void changeAmount(int difference) throws RemoteException {
		this.money += difference;
	}

	public int getMoney() {
		return money;
	}

	public boolean isGameEnd() throws RemoteException {
		return gameEnd;
	}

	public void setGameEnd(boolean bool) throws RemoteException {
		this.gameEnd = bool;
	}
}
