package pl.edu.agh.sr.blackjack.api;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface BlackJack extends Remote {

	void reigster(String nick, Listener listener) throws RemoteException;
	
	void unregister(String nick) throws RemoteException;

	void startGame(String nick, int bid) throws RemoteException;

	void hit(String nick) throws RemoteException;
	
	void stand(String nick) throws RemoteException;

	boolean isStand(String nick) throws RemoteException;

	boolean isGameEnd() throws RemoteException;

	void doubleDown(String nick) throws RemoteException;

	void split(String nick) throws RemoteException;

	void insurence(String nick) throws RemoteException;
}
