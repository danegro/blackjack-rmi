package pl.edu.agh.sr.blackjack.api;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Listener extends Remote {

	void onNewAction(String msg) throws RemoteException;
	
	void changeAmount(int difference) throws RemoteException;
	
	boolean isGameEnd() throws RemoteException;
	
	void setGameEnd(boolean bool) throws RemoteException;
}
