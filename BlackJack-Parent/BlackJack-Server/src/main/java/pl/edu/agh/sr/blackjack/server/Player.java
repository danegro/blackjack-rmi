package pl.edu.agh.sr.blackjack.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pl.edu.agh.sr.blackjack.api.Listener;

public class Player implements Comparable<Player>{

	private Listener listener;
	private List<Integer> cards;
	private int sum;
	private boolean lost;
	private int bid;
	private boolean ready;
	private int round;
	private boolean stand;
	private boolean state;

	public Player() {
		cards = new ArrayList<Integer>();
	}

	public Player(Listener listener) {
		cards = new ArrayList<Integer>();
		this.listener = listener;
	}

	public Listener getListener() {
		return listener;
	}

	public void addCard(int i) {
		cards.add(i);
	}

	public List<Integer> getCards() {
		return cards;
	}

	public int sumCards() {
		Collections.sort(cards);
		if (cards.size() == 2 && cards.get(0) == 1 && cards.get(1) >= 10) {
			sum = 666;
			return sum;
		}
		else {
			int sumTmp = 0;
			int acesCounter = 0;
			for (Integer i : cards)
				if (i == 1)
					acesCounter++;
				else if (i > 10)
					sumTmp += 10;
				else
					sumTmp += i;
			for (int i = 0; i < acesCounter; ++i)
				if (sumTmp + 11 > 21)
					sumTmp++;
				else
					sumTmp += 11;
			sum = sumTmp;
			return sum;
		}
	}

	public int getSum() {
		return sum;
	}

	public void setBid(int bid) {
		this.bid = bid;
	}

	public int getBid() {
		return bid;
	}

	public void setStand(boolean b) {
		this.stand = b;
	}

	public boolean isStand() {
		return stand;
	}

	public int compareTo(Player o) {
		return new Integer(this.sum).compareTo(new Integer(o.sum));
	}

	public void setReady(boolean b) {
		this.ready = b;
	}

	public boolean isReady() {
		return ready;
	}

	public void setRound(int i) {
		this.round = i;
	}

	public void incRound() {
		round++;
	}

	public int getRound() {
		return round;
	}

	public void setState(boolean b) {
		this.state = b;
	}

	public boolean isState() {
		return state;
	}

	public void setLost(boolean b) {
		this.lost = b;
	}

	public boolean isLost() {
		return lost;
	}

	public void clearCards() {
		cards = new ArrayList<Integer>();
	}

}
