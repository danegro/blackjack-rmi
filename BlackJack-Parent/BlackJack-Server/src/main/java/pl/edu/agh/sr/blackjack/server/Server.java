package pl.edu.agh.sr.blackjack.server;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

import pl.edu.agh.sr.blackjack.api.BlackJack;

public class Server {

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Usage: java Server host port");
			System.exit(0);
		}

		StringBuilder buffer = new StringBuilder("rmi://");
		buffer.append(args[0]);
		buffer.append(":");
		buffer.append(args[1]);
		buffer.append("/");

		int port = Integer.parseInt(args[1]);
		
		BlackJackImpl impl = new BlackJackImpl();

		try {
			BlackJack blackjack = (BlackJack) UnicastRemoteObject.exportObject(
					impl, 0);
			LocateRegistry.createRegistry(port);
			Naming.rebind(buffer.toString() + "blackjack", blackjack);
			System.out.println("Server started.");
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

}
