package pl.edu.agh.sr.blackjack.server;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import pl.edu.agh.sr.blackjack.api.BlackJack;
import pl.edu.agh.sr.blackjack.api.Listener;

public class BlackJackImpl implements BlackJack {

	private HashMap<String, Player> users = new HashMap<String, Player>();
	private Dealer dealer = new Dealer();
	private boolean gameInProgress = false;
	private int round = 0;

	public void reigster(String nick, Listener listener) throws RemoteException {
		users.put(nick, new Player(listener));
		System.out.println(nick + " registered.");
	}

	public void unregister(String nick) throws RemoteException {
		users.remove(nick);
		System.out.println(nick + " unregistered.");
	}

	public void startGame(String nick, int bid) throws RemoteException {
		users.get(nick).setBid(bid);
		users.get(nick).setReady(true);
		users.get(nick).setLost(false);
		users.get(nick).setState(false);
		users.get(nick).setRound(0);
		if (!gameInProgress && allReady()) {
			System.out.println("NEW GAME!");
			dealer = new Dealer();
			for (String key : users.keySet()) {
				users.get(key).getListener().onNewAction("Game started.\n");
				users.get(key).setReady(false);
				users.get(key).clearCards();
			}
			generateCards();
			printCards("X ");
			gameInProgress = true;
			round = 0;
		} else
			users.get(nick).getListener()
					.onNewAction("Waiting for all players.\n");
	}

	public void hit(String nick) throws RemoteException {
		if (gameInProgress && users.get(nick).getRound() == round) {
			users.get(nick).addCard(randomCard());
			printCards("X ");
			users.get(nick).incRound();
			if (isRoundEnd())
				endRound();
			users.get(nick).sumCards();
			if (users.get(nick).getSum() > 21)
				if (users.get(nick).getSum() != 666) {
					users.get(nick).setStand(true);
					users.get(nick).setLost(true);
					users.get(nick).getListener().onNewAction("Lost!\n");
				} else
					users.get(nick).setStand(true);
		}
	}

	public void stand(String nick) throws RemoteException {
		if (gameInProgress && users.get(nick).getRound() == round) {
			users.get(nick).incRound();
			users.get(nick).setStand(true);
			if (isRoundEnd())
				endRound();
		}
	}

	public boolean isStand(String nick) throws RemoteException {
		if (gameInProgress)
			return users.get(nick).isStand();
		else if (users.get(nick).getListener().isGameEnd())
			return true;
		else			
			return false;
	}

	public boolean isGameEnd() throws RemoteException {
		synchronized (this) {
			if (gameInProgress) {
				for (String nick : users.keySet())
					if (!users.get(nick).isStand() && !users.get(nick).isLost())
						return false;
				if (arePlayersLost()) {
					for (String nick : users.keySet()) {
						users.get(nick).getListener()
								.onNewAction("Dealer won.\n");
						users.get(nick).getListener()
								.onNewAction("You lose...\n");
						users.get(nick).setStand(false);
						users.get(nick).getListener()
								.changeAmount(-1 * users.get(nick).getBid());
						users.get(nick).getListener().setGameEnd(true);
					}
					gameInProgress = false;
					return true;
				} else {
					Integer hiddenCard = randomCard();
					printCards(parseCard(hiddenCard).toString());
					dealer.addCard(hiddenCard);
					while (dealer.sumCards() < 17) {
						dealer.addCard(randomCard());
						printCards("");
					}
					ArrayList<Player> toCompareList = new ArrayList<Player>();
					if (dealer.sumCards() == 666)
						dealer.setState(true);
					else if (dealer.getSum() <= 21)
						toCompareList.add(dealer);
					for (String nick : users.keySet()) {
						if (users.get(nick).sumCards() == 666)
							users.get(nick).setState(true);
						else if (users.get(nick).getSum() <= 21)
							toCompareList.add(users.get(nick));
					}
					Collections.sort(toCompareList);
					int dealerIndex = toCompareList.indexOf(dealer);
					for (String nick : users.keySet()) {
						users.get(nick).setStand(false);
						users.get(nick).getListener().setGameEnd(true);
						if (users.get(nick).isState()) {
							int amount = (int) (users.get(nick).getBid() * 1.5);
							users.get(nick).getListener()
									.onNewAction("You won!\n" + amount + "$");
							users.get(nick).getListener().changeAmount(amount);
						} else if (toCompareList.indexOf(users.get(nick)) > dealerIndex) {
							users.get(nick)
									.getListener()
									.onNewAction(
											"You won!\n"
													+ users.get(nick).getBid()
													+ "$\n");
							users.get(nick).getListener()
									.changeAmount(users.get(nick).getBid());
						} else if (toCompareList.indexOf(users.get(nick)) == dealerIndex
								&& dealerIndex != -1) {
							users.get(nick).getListener()
									.onNewAction("Draw.\n");
						} else {
							users.get(nick).getListener()
									.onNewAction("You lose...\n");
							users.get(nick)
									.getListener()
									.changeAmount(-1 * users.get(nick).getBid());
						}
					}
					gameInProgress = false;
					return true;
				}
			} else
				return false;
		}
	}

	public void doubleDown(String nick) throws RemoteException {
		throw new UnsupportedOperationException();
	}

	public void split(String nick) throws RemoteException {
		throw new UnsupportedOperationException();
	}

	public void insurence(String nick) throws RemoteException {
		throw new UnsupportedOperationException();
	}

	private void printCards(String hiddenCard) {
		StringBuilder msg = new StringBuilder("Cards: \n");
		msg.append("Dealer:\n");
		msg.append(hiddenCard);
		for (Integer i : dealer.getCards())
			msg.append(parseCard(i));
		msg.append("\n");
		for (String nick : users.keySet()) {
			msg.append("Player " + nick + ":\n");
			for (Integer i : users.get(nick).getCards()) {
				msg.append(parseCard(i));
			}
			msg.append("\n");
		}
		for (String nick : users.keySet())
			try {
				users.get(nick).getListener().onNewAction(msg.toString());
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	private String parseCard(Integer i) {
		if (i == 1 || i > 10) {
			String tmp = null;
			switch (i) {
			case 1:
				tmp = "A";
				break;
			case 11:
				tmp = "J";
				break;
			case 12:
				tmp = "Q";
				break;
			case 13:
				tmp = "K";
				break;
			}
			return (tmp + " ");
		} else
			return (i + " ");
	}

	private void generateCards() {
		dealer.addCard(randomCard());
		for (String nick : users.keySet()) {
			users.get(nick).addCard(randomCard());
			users.get(nick).addCard(randomCard());
		}
	}

	private int randomCard() {
		Random random = new Random();
		return random.nextInt(13) + 1;
	}

	private boolean allReady() {
		for (String nick : users.keySet())
			if (!users.get(nick).isReady())
				return false;
		return true;
	}

	private void endRound() {
		for (String nick : users.keySet())
			try {
				if (!users.get(nick).isStand() || !users.get(nick).isLost())
					users.get(nick).getListener().onNewAction("New round!\n");
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		++round;
	}

	private boolean isRoundEnd() {
		for (String nick : users.keySet())
			if (!users.get(nick).isStand()
					&& users.get(nick).getRound() != round + 1
					&& !users.get(nick).isLost())
				return false;
		return true;
	}

	private boolean arePlayersLost() {
		for (String nick : users.keySet())
			if (!users.get(nick).isLost())
				return false;
		return true;
	}

}
